############## problem 01 ##############

# Given a list, write a Python program to swap first and last element of the list.
# Example:
# Input : [1, 2, 3]
# Output : [3, 2, 1]

# Approach # 1:
# Find the length of the list and simply swap the first element with (n-1)th element.
# Swap function
def swapList(newList):
    size = len(newList)
    # Swapping
    temp = newList[0]
    newList[0] = newList[size - 1]
    newList[size - 1] = temp
    return newList
# Driver code
newList = [12, 35, 9, 56, 24]

print(swapList(newList))

# Approach 2: The last element of the list can be referred as list[-1].
# Therefore, we can simply swap list[0] with list[-1].
# Swap function
def swapList(newList):
    newList[0], newList[-1] = newList[-1], newList[0]
    return newList
# Driver code
newList = [12, 35, 9, 56, 24]
print(swapList(newList))

# Approach 3:
# Swap the first and last element is using tuple variable.
# Store the first and last element as a pair in a tuple variable, say get, and unpack those elements with first and
# last element in that list. Now, the First and last values in that list are swapped.

# Swap function
def swapList(list):
    get = list[-1], list[0]
    list[0], list[-1] = get
    return list
newList = [12, 35, 9, 56, 24]
print(swapList(newList))

# Approach  4:
# Using * operand.

list = [1, 2, 3, 4]
a, *b, c = list
print(a)
print(b)
print(c)

# Now let’s see the implementation of above approach:

# Swap function
def swapList(list):
    start, *middle, end = list
    list = [end, *middle, start]
    return list
# Driver code
newList = [12, 35, 9, 56, 24]
print(swapList(newList))

# Approach 5:
# Swap the first and last elements is to use the inbuilt function list.pop().
# Pop the first element and store it in a variable. Similarly, pop the last element and store it in another variable.
# Now insert the two popped element at each other’s original position.

# Swap function
def swapList(list):
    first = list.pop(0)
    last = list.pop(-1)

    list.insert(0, last)
    list.append(first)
    return list
# Driver code
newList = [12, 35, 9, 56, 24]
print(swapList(newList))


############ Python program to swap two elements in a list ############

# Approach 1:
# Simple swap, using comma assignment
# Since the positions of the elements are known, we can simply swap the positions of the elements

# Python3 program to swap elements
# at given positions

# Swap function
def swapPositions(list, pos1, pos2):
    list[pos1], list[pos2] = list[pos2], list[pos1]
    return list
# Driver function
List = [23, 65, 19, 90]
pos1 = list[0]
pos2 = list[2]
print(swapPositions(List, pos1-1, pos2-1))

# Approach 2 :
# Using Inbuilt list.pop() function
# Pop the element at pos1 and store it in a variable.
# Similarly, pop the element at pos2 and store it in another variable.
# Now insert the two popped element at each other’s original position.

# Python3 program to swap elements
# at given positions

# Swap function
def swapPositions(list, pos1, pos2):
    # popping both the elements from list
    first_ele = list.pop(pos1)
    second_ele = list.pop(pos2 - 1)

    # inserting in each other positions
    list.insert(pos1, second_ele)
    list.insert(pos2, first_ele)

    return list
# Driver function
List = [23, 65, 19, 90]
pos1 = list[0]
pos2 = list[2]
print(swapPositions(List, pos1 - 1, pos2 - 1))

# Approach 3 :
# Using tuple variable
# Store the element at pos1 and pos2 as a pair in a tuple variable, say get.
# Unpack those elements with pos2 and pos1 positions in that list.
# Now, both the positions in that list are swapped.

# Python3 program to swap elements at
# given positions

# Swap function
def swapPositions(list, pos1, pos2):
    # Storing the two elements
    # as a pair in a tuple variable get
    get = list[pos1], list[pos2]

    # unpacking those elements
    list[pos2], list[pos1] = get

    return list
List = [23, 65, 19, 90]

pos1 = list[0]
pos2 = list[2]
print(swapPositions(List, pos1 - 1, pos2 - 1))

# Approach 4:
# Using temp variable

# Swap function
def swapPositions(lis, pos1, pos2):
    temp = lis[pos1]
    lis[pos1] = lis[pos2]
    lis[pos2] = temp
    return lis

# Driver function
List = [23, 65, 19, 90]
pos1, pos2 = 1, 3

print(swapPositions(List, pos1 - 1, pos2 - 1))
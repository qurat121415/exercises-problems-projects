# Method 1:
# This is the naive approach where we will compare two numbers using if-else statement
def maximum(a, b):
    if a >= b:
        return a
    else:
        return b
# Driver code
a = 2
b = 4
print(maximum(a, b))

# Method 2: Using max() function
# This function is used to find the maximum of the values passed as its arguments.
a = 2
b = 4
maximum = max(a, b)
print(maximum)

# Method 3: Using Ternary Operator

# This operator is also known as conditional expression are operators that evaluate something based on-
# a condition being true or false. It simply allows testing a condition in a single line
a = 2
b = 4
# Use of ternary operator
print(a if a >= b else b)

# Method 4: Using lambda function
a = 2 ; b = 4
maximum = lambda a,b:a if a > b else b
print(f'{maximum(a,b)} is a maximum number')

# Method 5: Using list comprehension
a = 2 ; b = 4
x = [a if a > b else b]
print("maximum number is:",x)

# Minimum of two numbers
# Method 1: naive approach
# using if-else statement and will print the output accordingly
def minimum(a, b):
    if a <= b:
        return a
    else:
        return b
# Driver code
a = 2
b = 4
print(minimum(a, b))

# Method 2: Using min() function
#
# This function is used to find the minimum of the values passed as its arguments.
a = 2
b = 4

minimum = min(a, b)
print(minimum)

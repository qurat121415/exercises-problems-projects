# create a dictionary similar to the real-world dictionary.
# no limit to the definition you provide to any word
# The details and functionalities that are essential and must be present are:

# The user will give the word as input.
# Suppose that the word is present in your dictionary along with its definition or meaning.
# The program will print the meaning or definition of that word.

dictionary = {"father name" : "syed muhammad hassanain",
              "mother name" : "bibi masoma",
              "zulqarnain" : "first brother",
              "arifain" : "2nd brother",
              "taybain" : "3rd brother",
              "quratulain" : "sister of 3 brothers",
              "noorulain" : "last one, sister of 3 brothers and 1 sister"}
user = str(input("enter name: "))
print(dictionary[user])

# OR

dict = {'int' : 'any umber without decimal points',
        'float' : 'any number with decimal points',
        'str' : 'anything written in quotation',
        'char' : 'special characters or any characters'}
word = input("enter a word")
print("the meaning of "+word+" is:", dict.get(word))


# ############ EXERCISE 2 ############

# "Faulty Calculator"
# All the results will be correct, except for these few:
# 45 * 3 = 555
# 56+9 = 77
# 56/6 = 4
# 45 - 12 = 35

choose = "+, -, *, / "
user = input("choose an operator: \n" + choose)
n1 = int(input("enter first number: "))
n2 = int(input("enter second number: "))
if user == "+":
    if n1 == 56 and n2 == 9:
        print(77)
    else:
        print("sum of "+str(n1)+" and "+str(n2)+" is:", n1 + n2)
if user == "-":
    if n1 == 45 and n2 == 12:
        print(35)
    else:
        print("sum of "+str(n1)+" and "+str(n2)+" is:", n1 - n2)
if user == "*":
    if n1 == 45 and n2 == 3:
        print(555)
    else:
        print("sum of "+str(n1)+" and "+str(n2)+" is:", n1 * n2)
if user == "/":
    if n1 == 56 and n2 == 6:
        print(4)
    else:
        print("sum of "+str(n1)+" and "+str(n2)+" is:", n1 / n2)
print("Thank you")


# ############ EXERCISE 3 ############

# Guess The Number
# 1. The number of guesses should be limited, i.e (5 or 9).
# 2. Print the number of guesses left.
# 3. Print the number of guesses he took to win the game.
# 4. “Game Over” message should display
#     if the number of guesses becomes equal to 0.

n = 18
num_of_guesses = 6
while num_of_guesses > 0:
    user = int(input("guess a number: "))
    if user > n:
        print("you entered a greater number")
    elif user < 18:
        print("you entered a smaller number")
    else:
        print("you won")
        print("numer of guesses he took to finish: ", num_of_guesses)
        break
    print("number of guesses left: ", num_of_guesses)
    num_of_guesses = num_of_guesses - 1

if num_of_guesses == 0:
    print("game over")


# ############ EXERCISE 4 ############

# Astrologer's Star
#
user = int(input("enter number of rows you want: "))
user2 = int(input("type 1 or 0 \n"))
bol = bool(user2)
if bol == True:
    for i in range(user):
        for j in range(i + 1):
            print("* ", end="")
        print()
if bol == False:
    for i in range(user):
        for j in range(user - i):
            print("* ", end="")
        print()



############# EXERCISE 5 ############

# health management
# Create a food log file for each client
# Create an exercise log file for each client.
# Ask the user whether they want to log or retrieve client data.
# Write a function that takes the user input of the client's name.
# After the client's name is entered, A message should display "What you want to log. Diet or Exercise"
# Use function


def getdate():
    import datetime
    return datetime.datetime.now()

def log(take):
    if take == "Q" or take == "q":
        user_selection = int(input("press 1 for food log and 2 for exercise log: "))
        if user_selection == 1:
            food_log = input("type here\n")
            with open("qurat.food.txt", "a") as file:
                file.write(str([str(getdate())])+": "+food_log+"\n")
                print("successfully done")
            file.close()
        elif user_selection == 2:
            ex_log = input("type here\n")
            with open("qurat.ex.txt", "a") as file:
                file.write(str([str(getdate())])+": "+ex_log+"\n")
                print("successfully done")
            file.close()
    elif take == "H" or take == "h" :
        user_selection = int(input("press 1 for food log and 2 for exercise log: "))
        if user_selection == 1:
            food_log = input("type here\n")
            with open("huma.food.txt") as file:
                file.write(str([str(getdate())])+": "+food_log+"\n")
                print("successfully written")
            file.close()
        elif user_selection == 2:
            ex_log = input("type here\n")
            with open("huma.ex.txt", "a") as file:
                file.write(str([str(getdate())])+": "+ex_log+"\n")
                print("successfully witten")
            file.close()
    elif take == "A" or take == "a":
        user_selection = int(input("press 1 for food log and 2 for exercise log: "))
        if user_selection == 1:
            food_log = input("type here\n")
            with open("asia.food.txt", "a") as file:
                file.write(str([str(getdate())])+": "+food_log+"\n")
                print("successfully written")
            file.close()
        elif user_selection == 2:
            ex_log = input("type here\n")
            with open("asia.ex.txt", "a") as file:
                file.write(str([str(getdate())]) + ": " + ex_log + "\n")
                print("successfully written")
            file.close()
    else:
        print("plz enter valid input (Q(qurat),H(huma),A(asia)")

def retrieve(take):
    if take == "Q" or take == "q":
        user_selection = int(input("press 1 for food log and 2 for exercise log: "))
        if user_selection == 1:
            with open("qurat.food.txt") as file:
                for elements in file:
                    print(elements, end="")
            file.close()
        elif user_selection == 2:
            with open("qurat.ex.txt") as file:
                for elements in file:
                    print(elements, end="")
            file.close()
    elif take == "H" or take == "h":
        user_selection = int(input("press 1 for food log and 2 for exercise log: "))
        if user_selection == 1:
            with open("huma.food.txt") as file:
                for elements in file:
                    print(elements, end="")
            file.close()
        elif user_selection == 2:
            with open("huma.ex.txt") as file:
                for elements in file:
                    print(elements, end="")
            file.close()
    elif take == "A" or take == "a":
        user_selection = int(input("press 1 for food log and 2 for exercise log: "))
        if user_selection == 1:
            with open("asia.food.txt") as file:
                for elements in file:
                    print(elements, end="")
            file.close()
        elif user_selection == 2:
            with open("asia.ex.txt") as file:
                for elements in file:
                    print(elements, end="")
            file.close()
    else:
        print("plz enter valid input (Q(qurat),H(huma),A(asia)")

print("health management system: ")
choose_log_or_retrieve = input("press 'L' for log and 'R' for retrieve: ")
if choose_log_or_retrieve == "L" or choose_log_or_retrieve == "l":
    choose_Q_H_A = input("press 'Q/q' to quratulain, press 'H/h' to huma and press 'A/a' to asia: ")
    log(choose_Q_H_A)
elif choose_log_or_retrieve == "R" or choose_log_or_retrieve == "r":
    choose_Q_H_A = input("press 'Q/q' to quratulain, press 'H/h' to huma and press 'A/a' to asia: ")
    retrieve(choose_Q_H_A)


############# EXERCISE 6 ############

# Game Development: Snake Water Gun

# there are three objects, snake, water, and gun. So, the result will be
# Snake vs. Water: Snake drinks the water hence wins.
# Water vs. Gun: The gun will drown in water, hence a point for water
# Gun vs. Snake: Gun will kill the snake and win.
# In situations where both players choose the same object, the result will be a draw.
#
# Now moving on to instructions:
# You have to use a random choice function that we studied in tutorial #38, to select between, snake, water, and gun.
# You do not have to use a print statement in case of the above function.
# Then you have to give input from your side.
# After getting ten consecutive inputs, the computer will show the result based on each iteration.
# You have to use loops(while loop is preferred).

import random
ist = ['s', 'w', 'g']

human_chances = 10
no_of_chances = 0
computer_points = 0
human_points = 0

print("\t\t\t SNAKE WATER GUN \t\t\t\n")
print("s for snake \nw for gun \ng for gun")

while no_of_chances < human_chances:
    user_selection = input('Snake,Water,Gun: ')
    _random = random.choice(ist)

    if user_selection == _random:
        print("Tie Both 0 point to each \n ")
# if user choose s
    elif user_selection == "s" and _random == "g":
        computer_points = computer_points + 1
        print(f"your guess is {user_selection} and computer guess is {_random}")
        print("computer points increase by 1")
        print(f"computer point is {computer_points} and your point is {human_points}")
    elif user_selection == "s" and _random == "w":
        human_points = human_points + 1
        print(f"your guess is {user_selection} and computer guess is {_random}")
        print("human points increase by 1")
        print(f"computer point is {computer_points} and your point is {human_points}")

# if user choose w
    elif user_selection == "w" and _random == "s":
        computer_points = computer_points + 1
        print(f"your guess is {user_selection} and computer guess is {_random}")
        print("computer points increase by 1")
        print(f"computer point is {computer_points} and your point is {human_points}")
    elif user_selection == "w" and _random == "g":
        human_points = human_points + 1
        print(f"your guess is {user_selection} and computer guess is {_random}")
        print("human points increase by 1")
        print(f"computer point is {computer_points} and your point is {human_points}")

# if user choose g
    elif user_selection == "g" and _random == "w":
        computer_points = computer_points + 1
        print(f"your guess is {user_selection} and computer guess is {_random}")
        print("computer points increase by 1")
        print(f"computer point is {computer_points} and your point is {human_points}")
    elif user_selection == "g" and _random == "s":
        human_points = human_points + 1
        print(f"your guess is {user_selection} and computer guess is {_random}")
        print("human points increase by 1")
        print(f"computer point is {computer_points} and your point is {human_points}")
    else:
        print("you have input wrong \n")

    no_of_chances = no_of_chances + 1
    print("number of chances left", human_chances - no_of_chances, "out of", human_chances)

print("game over")
if computer_points == human_points:
    print("tie")
elif computer_points > human_points:
    print("computer wins and you loose")
else:
    print("you win and computer loose")

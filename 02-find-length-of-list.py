# Approach 1:
# Python code to demonstrate
# length of list
# using naive method

# Initializing list
test_list = [1, 4, 5, 7, 8]

# Printing test_list
print("The list is : " + str(test_list))

# Finding length of list
# using loop
# Initializing counter
counter = 0
for i in test_list:
    # incrementing counter
    counter = counter + 1

# Printing length of list
print("Length of list using naive method is : " + str(counter))

# Approach 2 :
# Using len()
# The len() method offers the most used and easy way to find the length of any list.
# This is the most conventional technique adopted by all programmers today.

# Python program to demonstrate working
# of len()
a = []
a.append("Hello")
a.append("I")
a.append("am")
a.append("Qurat Ul Ain")
print("The length of list is: ", len(a))
# OR
# Python program to demonstrate working
# of len()
n = len(["Hello", "I", "Am", "Qurat Ul Ain"])
print("The length of list is: ", n)

# Python program to demonstrate working
# of len()
z = len([10, 20, 30, 40])
print("The length of list is: ", z)

# Approach 3:
# Using length_hint()
# This technique is a lesser-known technique for finding list length.
# This particular method is defined in the operator class, and it can also tell the no. of elements present in the list.
# Code #2 : Demonstrating finding length of list using len() and length_hint()

# Python code to demonstrate
# length of list
# using len() and length_hint
from operator import length_hint

# Initializing list
test_list = [1, 4, 5, 7, 8]

# Printing test_list
print("The list is : " + str(test_list))

# Finding length of list
# using len()
list_len = len(test_list)

# Finding length of list
# using length_hint()
list_len_hint = length_hint(test_list)

# Printing length of list
print("Length of list using len() is : " + str(list_len))
print("Length of list using length_hint() is : " + str(list_len_hint))

# Performance Analysis
# a time analysis of how much time it takes to execute all of them to offer a better choice to use

from operator import length_hint
import time

# Initializing list
test_list = [1, 4, 5, 7, 8]

# Printing test_list
print("The list is : " + str(test_list))

# Finding length of list
# using loop
# Initializing counter
start_time_naive = time.time()
counter = 0
for i in test_list:
    # incrementing counter
    counter = counter + 1
end_time_naive = str(time.time() - start_time_naive)

# Finding length of list
# using len()
start_time_len = time.time()
list_len = len(test_list)
end_time_len = str(time.time() - start_time_len)

# Finding length of list
# using length_hint()
start_time_hint = time.time()
list_len_hint = length_hint(test_list)
end_time_hint = str(time.time() - start_time_hint)

# Printing Times of each
print("Time taken using naive method is : " + end_time_naive)
print("Time taken using len() is : " + end_time_len)
print("Time taken using length_hint() is : " + end_time_hint)

# Approach 4:
# Using sum()
# Use iteration inside the sum and each iteration add one and at the end of the iteration,
# we get the total length of the list.
# Python code to demonstrate
# length of list
# using sum()

# Initializing list
test_list = [1, 4, 5, 7, 8]

# Printing test_list
print("The list is : " + str(test_list))

# Finding length of list
# using sum()
list_len = sum(1 for i in test_list)

# Printing length of list
print("Length of list using len() is : " + str(list_len))
print("Length of list using length_hint() is : " + str(list_len))

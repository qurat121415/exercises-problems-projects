# PROBLEM 01
# Given an integer,n , perform the following conditional actions:
# If n is odd, print Weird
# If n is even and in the inclusive range of 2 to 5, print Not Weird
# If n is even and in the inclusive range of 6 to 20, print Weird
# If n is even and greater than 20, print Not Weird

n = int(input())
if n % 2 == 1:
    print('Weird')
elif n % 2 == 0 and 2 <= n <= 5:
    print('Not Weird')
elif n % 2 == 0 and 6 <= n <= 20:
    print('Weird')
else:
    print('Not Weird')

# PROBLEM 02
# The provided code stub reads two integers from STDIN,  and . Add code to print three lines where:
# 1. The first line contains the sum of the two numbers.
# 2. The second line contains the difference of the two numbers (first - second).
# 3. The third line contains the product of the two numbers
a = int(input("enter first number: "))
b = int(input("enter second number: "))
print("sum of the two numbers: ",a+b)
print("difference of the two numbers",a-b)
print("product of the two numbers",a*b)

# PROBLEM 03
# The provided code stub reads two integers, a and b, from STDIN.
# Add logic to print two lines. The first line should contain the result of integer division, a // b.
# The second line should contain the result of float division, a / b.
# No rounding or formatting is necessary.
a = int(input())
b = int(input())
print(a//b)
print(a/b)

# PROBLEM 04
# The provided code stub reads and integer,n , from STDIN.
# For all non-negative integers i<n, print i square.
n = int(input())
for i in range(0, n):
    print(i ** 2)

# PROBLEM 05
# Without using any string methods, try to print the following:
# 123...n
# Note that "..." represents the consecutive values in between.
N = int(input())
print(*range(1,N+1), sep='')

# PROBLEM 06
# You are given a string and your task is to swap cases.
# In other words, convert all lowercase letters to uppercase letters and vice versa.
def swap_case(s):
    result = s.swapcase()
    return result